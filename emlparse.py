import json
import xmltodict
from firebase import Firebase
import time
import datetime
import boto.sns

sns_conn = boto.sns.connect_to_region('ap-southeast-2')
arn = 'arn:aws:sns:ap-southeast-2:260639043700:fe-2016-twitter-updates-topic'

marginal_divisions=['185', '237', '165', '157', '311']
twitter_updates=['165']

FIREBASE_URL = "https://federalelection-2016.firebaseio.com"

def value(eml):
    return int(eml['#text'])

def text_value(eml):
    return eml['#text']

def candidate_id(candidate_eml):
    return int(candidate_eml['eml:CandidateIdentifier']['@Id'])

def candidate_name(candidate_eml):
    return candidate_eml['eml:CandidateIdentifier']['eml:CandidateName']

def candidate_party_id(candidate_eml):
    if 'eml:AffiliationIdentifier' in candidate_eml:
      return candidate_eml['eml:AffiliationIdentifier']['@Id']
    else:
      return '0'

def candidate_party_name(candidate_eml):
    if 'eml:AffiliationIdentifier' in candidate_eml:
      return candidate_eml['eml:AffiliationIdentifier']['eml:RegisteredName']
    else:
      return 'Independent'

def candidate_party_code(candidate_eml):
    if 'eml:AffiliationIdentifier' in candidate_eml:
      return candidate_eml['eml:AffiliationIdentifier']['@ShortCode']
    else:
      return 'IND'

def eml_to_JSON(eml_file, type="media feed"):
    elect_data = xmltodict.parse(open(eml_file))

    if type == "media feed":
      analysis_json = {}
      firstprefs_json = {}
      twocandidate_json = {}
      twoparty_json = {}
      result_status = elect_data['MediaFeed']['Results']['@Phase']

      for election in elect_data['MediaFeed']['Results']['Election']:
          # House of Representative contests
          if 'House' in election:
              analysis = election['House']['Analysis']['National']
              enrolment = analysis['Enrolment']

              national_first_prefs = analysis['FirstPreferences']
              party_groups = national_first_prefs['PartyGroup']

              national_two_party_prefs = analysis['TwoPartyPreferred']
              two_party_coalition = national_two_party_prefs['Coalition']

              analysis_json['House'] = {
                      'national_enrolment': enrolment,
                      'result_phase': result_status,
                      'first_preferences': [
                          {
                              'party_id': party_group['PartyGroupIdentifier']['@Id'],
                              'party_name': party_group['PartyGroupIdentifier']['PartyGroupName'],
                              'party_short_code': party_group['PartyGroupIdentifier']['@ShortCode'],
                              'votes': value(party_group['Votes']),
                              'votes_percent': party_group['Votes']['@Percentage'],
                              'swing': party_group['Votes']['@Swing']
                          }
                          for party_group in party_groups
                      ],
                      'two_party_preference': [
                          {
                              'party_id': coalition['CoalitionIdentifier']['@Id'],
                              'party_name': coalition['CoalitionIdentifier']['CoalitionName'],
                              'party_short_code': coalition['CoalitionIdentifier']['@ShortCode'],
                              'votes': value(coalition['Votes']),
                              'votes_percent': coalition['Votes']['@Percentage'],
                              'swing': coalition['Votes']['@Swing']
                          }
                          for coalition in two_party_coalition
                      ]
              }

              for contest in election['House']['Contests']['Contest']:
                  marginal = 0
                  twitter = 0

                  if contest['eml:ContestIdentifier']['@Id'] in marginal_divisions:
                    marginal = 1

                  if contest['eml:ContestIdentifier']['@Id'] in twitter_updates:
                    twitter = 1

                  electorate_id = int(contest['eml:ContestIdentifier']['@Id'])
                  electorate_name = contest['eml:ContestIdentifier']['eml:ContestName']
                  electorate_state = contest['PollingDistrictIdentifier']['StateIdentifier']['@Id']

                  # First preference data
                  firstprefs = contest['FirstPreferences']
                  candidates = firstprefs['Candidate']
                  polling_places_expected = firstprefs['@PollingPlacesExpected']
                  polling_places_returned = firstprefs['@PollingPlacesReturned']

                  firstprefs_json[electorate_name] = {
                      'electorate_id': electorate_id,
                      'electorate_name': electorate_name,
                      'electorate_state': electorate_state,
                      'marginal': marginal,
                      'total': value(firstprefs['Total']['Votes']),
                      'formal': value(firstprefs['Formal']['Votes']),
                      'informal': value(firstprefs['Informal']['Votes']),
                      'polling_places_expected': polling_places_expected,
                      'polling_places_returned': polling_places_returned,
                      'candidates': [
                          {
                              'party_id': candidate_party_id(candidate),
                              'party_name': candidate_party_name(candidate),
                              'party_short_code': candidate_party_code(candidate),
                              'candidate_name': candidate_name(candidate),
                              'candidate_id': candidate_id(candidate),
                              'elected': candidate['Elected']['#text'],
                              'incumbent': candidate['Incumbent']['#text'],
                              'votes': value(candidate['Votes']),
                              'votes_percent': candidate['Votes']['@Percentage'],
                              'swing': candidate['Votes']['@Swing']
                          }
                          for candidate in candidates
                      ]
                  }

                  # Two Candidate Preferred data
                  twocand = contest['TwoCandidatePreferred']
                  tcp_polling_places_expected = twocand['@PollingPlacesExpected']
                  tcp_polling_places_returned = twocand['@PollingPlacesReturned']
                  restricted = '@Restricted' in twocand
                  maverick = '@Maverick' in twocand

                  if not(restricted) and not(maverick):
                      candidates = twocand['Candidate']
                  else:
                      candidates = []

                  twocandidate_json[electorate_name] = {
                      'electorate_id': electorate_id,
                      'electorate_name': electorate_name,
                      'electorate_state': electorate_state,
                      'marginal': marginal,
                      'twitter_updates': twitter,
                      'polling_places_expected': tcp_polling_places_expected,
                      'polling_places_returned': tcp_polling_places_returned,
                      'available': not(restricted) and not(maverick),
                      'candidates': [
                          {
                              'party_id': candidate_party_id(candidate),
                              'party_name': candidate_party_name(candidate),
                              'party_short_code': candidate_party_code(candidate),
                              'candidate_name': candidate_name(candidate),
                              'candidate_id': candidate_id(candidate),
                              'elected': candidate['Elected']['#text'],
                              'incumbent': candidate['Incumbent']['#text'],
                              'gain': candidate['Elected']['#text'] != candidate['Incumbent']['#text'],
                              'votes': value(candidate['Votes']),
                              'votes_percent': candidate['Votes']['@Percentage'],
                              'swing': candidate['Votes']['@Swing']
                          }
                          for candidate in candidates
                      ]
                  }

                  twoparty = contest['TwoPartyPreferred']
                  coalitions = twoparty['Coalition']

                  twoparty_json[electorate_name] = {
                      'electorate_id': electorate_id,
                      'electorate_name': electorate_name,
                      'electorate_state': electorate_state,
                      'marginal': marginal,
                      'polling_places_expected': tcp_polling_places_expected,
                      'polling_places_returned': tcp_polling_places_returned,
                      'coalition': [
                          {
                              'coalition_code': coalition['CoalitionIdentifier']['@ShortCode'],
                              'coalition_name': coalition['CoalitionIdentifier']['CoalitionName'],
                              'votes': value(coalition['Votes']),
                              'votes_percent': coalition['Votes']['@Percentage'],
                              'swing': coalition['Votes']['@Swing']
                          }
                          for coalition in coalitions
                      ]
                  }


      # Write JSON files
      json.dump(firstprefs_json, open('firstpreferences.json', 'w'))
      json.dump(twocandidate_json, open('twocandidate.json', 'w'))
      json.dump(twoparty_json, open('twoparty.json', 'w'))
      json.dump(analysis_json, open('analysis.json', 'w'))

      try:
        f1 = Firebase(FIREBASE_URL + '/analysis')
        r1 = f1.put(analysis_json)

        f2 = Firebase(FIREBASE_URL +  '/results')
        r2 = f2.put({'first_preferences':firstprefs_json, 'two_candidate_preferences':twocandidate_json, 'two_party_preferences': twoparty_json})

        result = sns_conn.publish(arn, {}, "Notify Update")
        console.log(result)
      except:
        pass

    elif type == "candidates":
      candidates_json = {}
      parties_json = {}

      for election in elect_data['EML']['CandidateList']['Election']:
        category = election['ElectionIdentifier']['ElectionCategory']

        # House of Representatives candidates
        if category == 'House':
          for electorate in election['Contest']:
            electorate_id = int(electorate['ContestIdentifier']['@Id'])

            for candidate in electorate['Candidate']:
              cand_id = int(candidate['CandidateIdentifier']['@Id'])

              candidates_json[cand_id] = {
                'candidate_id': cand_id,
                'electorate_id': electorate_id,
                'party_name': candidate['Affiliation']['AffiliationIdentifier']['RegisteredName'] if 'Affiliation' in candidate else None,
                'party_code': candidate['Affiliation']['AffiliationIdentifier']['@ShortCode'] if 'Affiliation' in candidate else None,
                'party_id': int(candidate['Affiliation']['AffiliationIdentifier']['@Id']) if 'Affiliation' in candidate else None,
                'name': candidate['CandidateIdentifier']['CandidateName'],
                'firstname': candidate['CandidateFullName']['xnl:PersonName']['xnl:FirstName'][0],
                'lastname': candidate['CandidateFullName']['xnl:PersonName']['xnl:LastName'],
                'gender': candidate['Gender'],
              }

              if 'Affiliation' in candidate:
                party_id = candidate['Affiliation']['AffiliationIdentifier']['@Id']
                party_code = candidate['Affiliation']['AffiliationIdentifier']['@ShortCode']
                parties_json[party_code] = {
                  'id': party_id,
                  'shortname': candidate['Affiliation']['AffiliationIdentifier']['@ShortCode'],
                  'name': candidate['Affiliation']['AffiliationIdentifier']['RegisteredName'],
                }

      json.dump(candidates_json, open('candidates.json', 'w'))
      json.dump(parties_json, open('parties.json', 'w'))

    elif type == "pollingdistricts":
      polling_booths_json = {}

      for pollingdistrict in elect_data['MediaFeed']['PollingDistrictList']['PollingDistrict']:
        pollingdistrict_electorate_name = pollingdistrict['PollingDistrictIdentifier']['Name']
        pollingdistrict_electorate_id = pollingdistrict['PollingDistrictIdentifier']['@Id']
        pollingdistrict_electorate_state = pollingdistrict['PollingDistrictIdentifier']['StateIdentifier']['@Id']
        pollingdistrict_pollingplaces = pollingdistrict['PollingPlaces']['PollingPlace']

        polling_booths_json[pollingdistrict_electorate_id] = {
                    'polling_place': [
                          {
                              'electorate_id': pollingdistrict_electorate_id,
                              'electorate_name': pollingdistrict_electorate_name,
                              'polling_place_type': pollingplace['PollingPlaceIdentifier']['@Classification'] if '@Classification' in pollingplace['PollingPlaceIdentifier'] else None,
                              'polling_place_id': pollingplace['PollingPlaceIdentifier']['@Id'],
                              'polling_place_name': pollingplace['PollingPlaceIdentifier']['@Name'],
                              'polling_place_lat': pollingplace['eml:PhysicalLocation']['eml:Address']['xal:PostalServiceElements']['xal:AddressLatitude'],
                              'polling_place_long': pollingplace['eml:PhysicalLocation']['eml:Address']['xal:PostalServiceElements']['xal:AddressLongitude'],
                              'polling_place_suburb': text_value(pollingplace['eml:PhysicalLocation']['eml:Address']['xal:AddressLines']['xal:AddressLine'][len(pollingplace['eml:PhysicalLocation']['eml:Address']['xal:AddressLines']['xal:AddressLine']) - 3]),
                              'polling_place_postcode': text_value(pollingplace['eml:PhysicalLocation']['eml:Address']['xal:AddressLines']['xal:AddressLine'][len(pollingplace['eml:PhysicalLocation']['eml:Address']['xal:AddressLines']['xal:AddressLine']) - 1])
                          }
                          for pollingplace in pollingdistrict_pollingplaces
                      ]
        }

        json.dump(polling_booths_json, open('polling_booths.json', 'w'))

